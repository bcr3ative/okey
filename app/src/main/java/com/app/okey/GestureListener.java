package com.app.okey;

import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class GestureListener implements View.OnTouchListener {

    private final GestureDetector mGestureDetector;

    private int mScrollSamples = 0;

    public GestureListener(Context context) {
        mGestureDetector = new GestureDetector(context, new Gestures());
    }

    // Public methods to override
    public void onDoubleTap() {}
    public void onDown() {}
    public void onSwipeLeft() {}
    public void onSwipeRight() {}
    public void onSwipeUp() {}
    public void onSwipeDown() {}
    public void onLongPress() {}
    public void onShowPress() {}
    public void onSingleTapConfirmed() {}
    public void onSingleTapUp() {}
    public void onScrollRight() {}
    public void onScrollLeft() {}

    public boolean onTouch(View v, MotionEvent event) {
        return mGestureDetector.onTouchEvent(event);
    }

    private final class Gestures extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_DISTANCE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;
        private static final int SCROLL_THRESHOLD = 6;

        /**
         * Notified when a double-tap occurs.
         * @param e The down motion event of the first tap of the double-tap.
         * @return true if the event is consumed, else false
         */
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            Log.d("GestureListener", "onDoubleTap");
            GestureListener.this.onDoubleTap();
            return true;
        }

        /**
         * Notified when a tap occurs with the down MotionEvent that triggered it.
         * This will be triggered immediately for every down event. All other events
         * should be preceded by this.
         * @param e The down motion event.
         * @return true if the event is consumed, else false
         */
        @Override
        public boolean onDown(MotionEvent e) {
            GestureListener.this.onDown();
            return true;
        }

        /**
         * Notified of a fling event when it occurs with the initial on down MotionEvent
         * and the matching up MotionEvent. The calculated velocity is supplied along the x and
         * y axis in pixels per second.
         * @param e1 The first down motion event that started the fling.
         * @param e2 The move motion event that triggered the current onFling.
         * @param velocityX The velocity of this fling measured in pixels per second along the x
         *                  axis.
         * @param velocityY The velocity of this fling measured in pixels per second along the y
         *                  axis.
         * @return true if the event is consumed, else false
         */
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            float distanceX = e2.getX() - e1.getX();
            float distanceY = e2.getY() - e1.getY();

            if (Math.abs(distanceX) > Math.abs(distanceY) && Math.abs(distanceX) > SWIPE_DISTANCE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                if (distanceX > 0) {
                    Log.d("GestureListener", "onSwipeRight");
                    GestureListener.this.onSwipeRight();
                } else {
                    Log.d("GestureListener", "onSwipeLeft");
                    GestureListener.this.onSwipeLeft();
                }
                return true;
            } else if (Math.abs(distanceY) > Math.abs(distanceX) && Math.abs(distanceY) > SWIPE_DISTANCE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                if (distanceY < 0) {
                    Log.d("GestureListener", "onSwipeUp");
                    GestureListener.this.onSwipeUp();
                } else {
                    Log.d("GestureListener", "onSwipeDown");
                    GestureListener.this.onSwipeDown();
                }
                return true;
            }
            return false;
        }

        /**
         * Notified when a long press occurs with the initial on down MotionEvent that trigged it.
         * @param e The initial on down motion event that started the longpress.
         */
        @Override
        public void onLongPress(MotionEvent e) {
            Log.d("GestureListener", "onLongPress");
            GestureListener.this.onLongPress();
        }

        /**
         * Notified when a scroll occurs with the initial on down MotionEvent and the current
         * move MotionEvent. The distance in x and y is also supplied for convenience.
         * @param e1 The first down motion event that started the scrolling.
         * @param e2 The move motion event that triggered the current onScroll.
         * @param distanceX The distance along the X axis that has been scrolled since the last
         *                  call to onScroll. This is NOT the distance between e1 and e2.
         * @param distanceY The distance along the Y axis that has been scrolled since the last
         *                  call to onScroll. This is NOT the distance between e1 and e2.
         * @return true if the event is consumed, else false
         */
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            Log.d("test", String.valueOf(distanceX));
            //Log.d("GestureListener", "onScroll");
            if (distanceX <= 0.0){
                mScrollSamples++;
                if (mScrollSamples > SCROLL_THRESHOLD) {
                    GestureListener.this.onScrollRight();
                    mScrollSamples = 0;
                }
            } else {
                mScrollSamples++;
                if (mScrollSamples > SCROLL_THRESHOLD) {
                    GestureListener.this.onScrollLeft();
                    mScrollSamples = 0;
                }
            }
            return true;
        }

        /**
         * The user has performed a down MotionEvent and not performed a move or up yet.
         * This event is commonly used to provide visual feedback to the user to let them
         * know that their action has been recognized i.e. highlight an element.
         * @param e The down motion event
         */
        @Override
        public void onShowPress(MotionEvent e) {
            Log.d("GestureListener", "onShowPress");
            GestureListener.this.onShowPress();
        }

        /**
         * Notified when a single-tap occurs.
         * Unlike onSingleTapUp(MotionEvent), this will only be called after the detector
         * is confident that the user's first tap is not followed by a second tap leading
         * to a double-tap gesture.
         * @param e The down motion event of the single-tap.
         * @return true if the event is consumed, else false
         */
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            Log.d("GestureListener", "onSingleTapConfirmed");
            GestureListener.this.onSingleTapConfirmed();
            return true;
        }

        /**
         * Notified when a tap occurs with the up MotionEvent that triggered it.
         * @param e The up motion event that completed the first tap
         * @return true if the event is consumed, else false
         */
        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            Log.d("GestureListener", "onSingleTapUp");
            GestureListener.this.onSingleTapUp();
            return true;
        }
    }
}