package com.app.okey;

public class KeyboardLayout {

    public static final int TEXT_LAYOUT = 1;
    public static final int TEXT_LAYOUT_UP = 2;
    public static final int SYMBOLS_LAYOUT = 3;

    private static final String[] textLayout = {
            "w", "e", "r", "f", "a", "s", "d",
            "h", "u", "i", "o", "j", "k", "l",
            "t", "y", "v", "q", "x", "c", ",",
            "g", "m", "z", ".", "b", "n", "p"
    };

    private static final String[] textLayoutUp = {
            "W","E","R","F","A","S","D",
            "H","U","I","O","J","K","L",
            "T","Y","V","Q","X","C",",",
            "G","M","Z",".","B","N","P"
    };

    private static final String[] symbolsLayout = {
            "1", "2", "3", "4", "0", "*", "+",
            "5", "6", "7", "8", "-", "/", "9",
            "!", "'", "(", "@", "%", "=", ";",
            ")", "\"", "?", ":", "_", "&", "#"
    };

    public static String[] getLayout(int layout) {

        switch (layout) {
            case 1:
                return textLayout;
            case 2:
                return textLayoutUp;
            case 3:
                return symbolsLayout;
            default:
                return textLayout;
        }
    }
}
