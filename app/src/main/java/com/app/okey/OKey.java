package com.app.okey;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.inputmethodservice.InputMethodService;
import android.util.Log;
import android.view.Gravity;
import android.view.HapticFeedbackConstants;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import java.util.ArrayList;

public class OKey extends InputMethodService implements View.OnTouchListener {

    private TextView mSpaceKey, mActionKey, mShiftKey, mDeleteKey, mSymbolsKey;

    private TextView[] mKeys = new TextView[28];

    private LinearLayout mKeyboardView;

    private Context mContext;

    private InputConnection mInputConnection;

    private int mImeAction, mKeyboardLayout, mTextLenght;

    private Dictionary mDict;

    private boolean mShiftLock;

    private CharSequence mText;

    private TextSwitcher mWordsView;

    private ArrayList<String> mWordsToShow = new ArrayList<>();
    private int mWordIndex = 0;

    /**
     * Called by the system when the service is first created. Do not call this method directly.
     */
    @Override
    public void onCreate() {
        super.onCreate();

        mContext = getApplicationContext();
        mDict = new Dictionary(mContext);
        SQLiteDatabase db = mDict.getWritableDatabase();
    }

    /**
     * This is a hook that subclasses can use to perform initialization of their interface.
     * It is called for you prior to any of your UI objects being created, both after the
     * service is first created and after a configuration change happens.
     */
    @Override
    public void onInitializeInterface() {
        super.onInitializeInterface();
    }

    /**
     * Create and return the view hierarchy used for the input area (such as a soft keyboard).
     * This will be called once, when the input area is first displayed.
     * You can return null to have no input area; the default implementation returns null.
     */
    @Override
    public View onCreateInputView() {

        mKeyboardView = (LinearLayout) getLayoutInflater().inflate(R.layout.layout2test, null);

        registerKeyListeners();
        registerSKeyListeners();

        return mKeyboardView;
    }

    /**
     * Called when a new client has bound to the input method.
     */
    @Override
    public void onBindInput() {
        super.onBindInput();

        Log.d("OKey", "Binded to client!");
    }

    /**
     * Called when the previous bound client is no longer associated with the input method.
     */
    @Override
    public void onUnbindInput() {
        super.onUnbindInput();

        Log.d("OKey", "Unbinded from client!");
    }

    /**
     * Called to inform the input method that text input has started in an editor.
     * You should use this callback to initialize the state of your input to match the state of
     * the editor given to it.
     * @param attribute The attributes of the editor that input is starting in.
     * @param restarting Set to true if input is restarting in the same editor such as because
     *                   the application has changed the text in the editor.
     *                   Otherwise will be false, indicating this is a new session with the editor.
     */
    @Override
    public void onStartInput(EditorInfo attribute, boolean restarting) {
        super.onStartInput(attribute, restarting);

        Log.d("OKey", "Connected to the editor!");
        mInputConnection = getCurrentInputConnection();
        mShiftLock = false;
    }

    /**
     * Called to inform the input method that text input has finished in the last editor.
     * At this point there may be a call to onStartInput(EditorInfo, boolean) to perform input
     * in a new editor, or the input method may be left idle.
     * This method is not called when input restarts in the same editor.
     */
    @Override
    public void onFinishInput() {
        super.onFinishInput();

        Log.d("OKey", "Disconnected from the editor!");
    }

    /**
     * Called when the input view is being shown and input has started on a new editor.
     * This will always be called after onStartInput(EditorInfo, boolean),
     * allowing you to do your general setup there and just view-specific setup here.
     * You are guaranteed that onCreateInputView() will have been called some time before
     * this function is called.
     */
    @Override
    public void onStartInputView(EditorInfo info, boolean restarting) {
        super.onStartInputView(info, restarting);

        mKeyboardLayout = KeyboardLayout.TEXT_LAYOUT;
        changeKeyboardLayout(mKeyboardLayout);
        mImeAction = setImeAction(info.imeOptions);
    }

    /**
     * Called when the application has reported a new selection region of the text.
     * This is called whether or not the input method has requested extracted text updates,
     * although if so it will not receive this call if the extracted text has changed as well.
     */
    @Override
    public void onUpdateSelection(int oldSelStart, int oldSelEnd, int newSelStart, int newSelEnd, int candidatesStart, int candidatesEnd) {
        super.onUpdateSelection(oldSelStart, oldSelEnd, newSelStart, newSelEnd, candidatesStart, candidatesEnd);
    }

    private void registerKeyListeners() {
        int keyId;

        for (int i = 1; i <= mKeys.length; i++) {
            keyId = getResources().getIdentifier("key" + String.valueOf(i), "id", getPackageName());
            mKeys[i-1] = (TextView) mKeyboardView.findViewById(keyId);
            mKeys[i-1].setHapticFeedbackEnabled(true);
            mKeys[i-1].setOnTouchListener(this);
        }
    }

    private void registerSKeyListeners() {

        mSpaceKey = (TextView) mKeyboardView.findViewById(R.id.space);
        mActionKey = (TextView) mKeyboardView.findViewById(R.id.action);
        mShiftKey = (TextView) mKeyboardView.findViewById(R.id.shift);
        mDeleteKey = (TextView) mKeyboardView.findViewById(R.id.delete);
        mSymbolsKey = (TextView) mKeyboardView.findViewById(R.id.symbols);
        mWordsView = (TextSwitcher) mKeyboardView.findViewById(R.id.words);

        handleSpace(mSpaceKey);
        handleAction(mActionKey);
        handleShift(mShiftKey);
        handleDelete(mDeleteKey);
        handleSymbols(mSymbolsKey);
        handleWords(mWordsView);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        TextView key = (TextView) v;

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            key.setBackgroundResource(R.drawable.key_ondown_bg);
            key.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            key.setBackgroundResource(0);
            String composed = key.getText().toString();
            mInputConnection.commitText(composed, composed.length());

            if (mShiftLock == false && mKeyboardLayout == KeyboardLayout.TEXT_LAYOUT_UP) {
                mKeyboardLayout = KeyboardLayout.TEXT_LAYOUT;
                changeKeyboardLayout(mKeyboardLayout);
            }
            setAnimInLeft();
            getWordList();
        }

        return true;
    }

    private void handleSpace(final TextView skey) {
        skey.setOnTouchListener(new GestureListener(mContext) {
            @Override
            public void onSingleTapUp() {
                mDict.addWeight(getUncompleteWord());
                mInputConnection.commitText(" ", 1);
                skey.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                mWordsToShow = new ArrayList<String>();
                mWordIndex = 0;
                mWordsView.setText("");
            }

            @Override
            public void onDown() {
                mText = mInputConnection.getTextBeforeCursor(30, 0);
                mTextLenght = mText.length();
            }

            @Override
            public void onScrollLeft() {
                if (mTextLenght != 0) {
                    mTextLenght = mTextLenght - 1;
                    Log.d("position", String.valueOf(mTextLenght));
                    mInputConnection.setSelection(mTextLenght, mTextLenght);
                }
            }

            @Override
            public void onScrollRight() {
                    mTextLenght = mTextLenght + 1;
                    Log.d("position", String.valueOf(mTextLenght));
                    mInputConnection.setSelection(mTextLenght, mTextLenght);

            }
        });
    }

    private void handleAction(final TextView skey) {
        skey.setOnTouchListener(new GestureListener(mContext) {
            @Override
            public void onSingleTapUp() {
                if (mImeAction == EditorInfo.IME_ACTION_UNSPECIFIED) {
                    // Return key
                    skey.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                    mInputConnection.commitText("\n", 1);
                } else {
                    mInputConnection.performEditorAction(mImeAction);
                }
            }
        });
    }

    private void handleShift(final TextView skey) {
        skey.setOnTouchListener(new GestureListener(mContext) {
            @Override
            public void onLongPress() {
                skey.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
                if (mKeyboardLayout == KeyboardLayout.TEXT_LAYOUT) {
                    mShiftLock = true;
                    mShiftKey.setBackgroundResource(R.drawable.skey_bg);
                    mKeyboardLayout = KeyboardLayout.TEXT_LAYOUT_UP;
                    changeKeyboardLayout(mKeyboardLayout);
                }
            }

            @Override
            public void onSingleTapUp() {
                skey.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                mShiftLock = false;
                mShiftKey.setBackgroundResource(0);
                if (mKeyboardLayout == KeyboardLayout.TEXT_LAYOUT) {
                    mKeyboardLayout = KeyboardLayout.TEXT_LAYOUT_UP;
                    changeKeyboardLayout(mKeyboardLayout);
                } else if (mKeyboardLayout == KeyboardLayout.TEXT_LAYOUT_UP) {
                    mKeyboardLayout = KeyboardLayout.TEXT_LAYOUT;
                    changeKeyboardLayout(mKeyboardLayout);
                }
            }
        });
    }

    private void handleDelete(final TextView skey) {
        skey.setOnTouchListener(new GestureListener(mContext) {
            @Override
            public void onDoubleTap() {
                mInputConnection.deleteSurroundingText(1, 0);
                skey.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                setAnimInRight();
                getWordList();
            }

            @Override
            public void onSingleTapUp() {
                mInputConnection.deleteSurroundingText(1, 0);
                skey.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                setAnimInRight();
                getWordList();
            }
        });
    }

    private void handleSymbols(final TextView skey) {
        skey.setOnTouchListener(new GestureListener(mContext) {
            @Override
            public void onSingleTapUp() {
                skey.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                if (mKeyboardLayout == KeyboardLayout.SYMBOLS_LAYOUT) {
                    mKeyboardLayout = KeyboardLayout.TEXT_LAYOUT;
                    changeKeyboardLayout(mKeyboardLayout);
                } else {
                    mShiftKey.setBackgroundResource(0);
                    mKeyboardLayout = KeyboardLayout.SYMBOLS_LAYOUT;
                    changeKeyboardLayout(mKeyboardLayout);
                }
            }
        });
    }

    private void handleWords(final TextSwitcher skey) {
        skey.setFactory(new ViewSwitcher.ViewFactory() {
            public View makeView() {
                TextView wordView = new TextView(mContext);
                wordView.setGravity(Gravity.CENTER_HORIZONTAL);
                wordView.setTextSize(18);
                wordView.setTextColor(Color.WHITE);
                return wordView;
            }
        });

        skey.setOnTouchListener(new GestureListener(mContext) {
            @Override
            public void onSingleTapUp() {
                if (mWordsToShow.size() != 0) {
                    String uncompleteWord = getUncompleteWord();
                    mInputConnection.deleteSurroundingText(uncompleteWord.length(), 0);
                    String chosenWord = mWordsToShow.get(mWordIndex);
                    mInputConnection.commitText(chosenWord, chosenWord.length());
                }
            }

            @Override
            public void onSwipeLeft() {
                if (mWordsToShow.size() != 0) {
                    setAnimInRight();

                    mWordIndex++;
                    // If index reaches maximum reset it
                    if (mWordIndex == mWordsToShow.size()) mWordIndex = 0;
                    mWordsView.setText(mWordsToShow.get(mWordIndex));
                }
            }

            @Override
            public void onSwipeRight() {
                if (mWordsToShow.size() != 0) {
                    setAnimInLeft();

                    mWordIndex--;
                    // If index reaches maximum reset it
                    if (mWordIndex == -1) mWordIndex = mWordsToShow.size() - 1;
                    mWordsView.setText(mWordsToShow.get(mWordIndex));
                }
            }
        });
    }

    /**
     * Updates the text of the ime action key according to editor info (Could be Return, Go, Send,
     * Search, Next,...). Returns the ime action flag.
     * @param imeAction Ime action before filtering.
     * @return Filtered ime action.
     */
    private int setImeAction(int imeAction) {
        int action;

        switch (imeAction & (EditorInfo.IME_MASK_ACTION | EditorInfo.IME_FLAG_NO_ENTER_ACTION)) {
            case EditorInfo.IME_ACTION_DONE:
                mActionKey.setText("Done");
                action = EditorInfo.IME_ACTION_DONE;
                break;
            case EditorInfo.IME_ACTION_GO:
                mActionKey.setText("Go");
                action = EditorInfo.IME_ACTION_GO;
                break;
            case EditorInfo.IME_ACTION_NEXT:
                mActionKey.setText("Next");
                action = EditorInfo.IME_ACTION_NEXT;
                break;
            case EditorInfo.IME_ACTION_SEARCH:
                mActionKey.setText("Search");
                action = EditorInfo.IME_ACTION_SEARCH;
                break;
            case EditorInfo.IME_ACTION_SEND:
                mActionKey.setText("Send");
                action = EditorInfo.IME_ACTION_SEND;
                break;
            default:
                mActionKey.setText("Return");
                action = EditorInfo.IME_ACTION_UNSPECIFIED;
                break;
        }

        return action;
    }

    /**
     * Method changes current layout to the one specified by the paremeter layout.
     * @param layout Integer which represents a layout.
     * @see KeyboardLayout
     */
    private void changeKeyboardLayout(int layout) {

        String[] keys = KeyboardLayout.getLayout(layout);

        for (int i = 0; i < keys.length; i++) {
            mKeys[i].setText(keys[i]);
        }
    }

    private String getUncompleteWord() {
        String uncompleteWord = "";
        String afterCursor = mInputConnection.getTextAfterCursor(1, 0).toString();
        String beforeCursor = mInputConnection.getTextBeforeCursor(30, 0).toString();
        int beforeLen = beforeCursor.length() - 1;
        int i;

        if (afterCursor.equals("") || afterCursor.equals(" ")) {
            for (i = beforeLen; i >= 0; i--) {
                if (beforeCursor.charAt(i) == '\0' || beforeCursor.charAt(i) == ' ') break;
            }

            uncompleteWord = beforeCursor.substring(i+1);
        }

        Log.d("Autocomplete", "Uncomplete word \"" + uncompleteWord + "\"");
        Log.d("Autocomplete", "------------");

        return uncompleteWord;
    }

    private void getWordList() {
        String uncompleteWord = getUncompleteWord();
        if (!uncompleteWord.equals("")) {
            mWordsToShow = mDict.getMatchedWords(uncompleteWord);
            Log.d("Autocomplete", mWordsToShow.toString());
            mWordIndex = 0;
            if (mWordsToShow.size() != 0) mWordsView.setText(mWordsToShow.get(mWordIndex));
        } else {
            mWordsToShow = new ArrayList<String>();
            mWordIndex = 0;
            mWordsView.setText("");
        }
    }

    private void setAnimInLeft() {
        // Declare the in and out animations and initialize them
        Animation in = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_out_right);

        // set the animation type of textSwitcher
        mWordsView.setInAnimation(in);
        mWordsView.setOutAnimation(out);
    }

    private void setAnimInRight() {
        // Declare the in and out animations and initialize them
        Animation in = AnimationUtils.loadAnimation(mContext, R.anim.slide_in_right);
        Animation out = AnimationUtils.loadAnimation(mContext, R.anim.slide_out_left);

        // set the animation type of textSwitcher
        mWordsView.setInAnimation(in);
        mWordsView.setOutAnimation(out);
    }
}