package com.app.okey;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;

public class Dictionary extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Dictionary.sqlite";
    public static final String TABLE_NAME = "dictionary";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_WORD = "word";
    public static final String COLUMN_WEIGHT = "weight";

    private Context mContext;

    public Dictionary(Context context) {
        super(context, DATABASE_NAME , null, 1);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE " + TABLE_NAME + " ("
                        + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + COLUMN_WORD + " TEXT NOT NULL, "
                        + COLUMN_WEIGHT + " INTEGER NOT NULL DEFAULT 0)"
        );
        populateDictionary(db);
        Log.d("Database", "Database created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void addWeight(String word) {
        String query = "UPDATE dictionary SET weight = weight + 1 WHERE word = '" + word.toLowerCase() + "'";
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(query);
    }

    public ArrayList<String> getMatchedWords(String pattern) {
        ArrayList<String> list = new ArrayList<>();
        String query = "SELECT word FROM " + TABLE_NAME + " WHERE word LIKE ? ORDER BY weight DESC LIMIT 10";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{ pattern + "%" });

        if (cursor.moveToFirst()) {
            do {
                list.add(cursor.getString(cursor.getColumnIndex(COLUMN_WORD)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return list;
    }

    public void deleteDatabase() {
        mContext.deleteDatabase(DATABASE_NAME);
        Log.d("Database", "Database deleted!");
    }

    public void backupDatabase() throws IOException {
        //Open your local db as the input stream
        File dbFile = mContext.getDatabasePath(DATABASE_NAME);
        Log.d("Database", "Exporting database...");
        FileInputStream fis = new FileInputStream(dbFile);

        String outFileName = Environment.getExternalStorageDirectory()+"/"+DATABASE_NAME;
        //Open the empty db as the output stream
        OutputStream output = new FileOutputStream(outFileName);
        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = fis.read(buffer))>0){
            output.write(buffer, 0, length);
        }
        //Close the streams
        output.flush();
        output.close();
        fis.close();
        Log.d("Database", "Exporting done!");
    }

    private void populateDictionary(SQLiteDatabase db) {
        Log.d("Database", "Populating dictionary...");
        String mCSVfile = "wordlist.txt";
        AssetManager manager = mContext.getAssets();
        InputStream inStream = null;
        try {
            inStream = manager.open(mCSVfile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedReader buffer = new BufferedReader(new InputStreamReader(inStream));
        String line = "";

        db.beginTransaction();
        try {
            while ((line = buffer.readLine()) != null) {
                ContentValues cv = new ContentValues(1);
                cv.put(COLUMN_WORD, line.trim());
                db.insert(TABLE_NAME, null, cv);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        Log.d("Database", "Populating done!");
    }
}
